#!/bin/bash
apt install   docker-ce  -y
cat > /etc/docker/daemon.json  <<EOF


{  
    "exec-opts": ["native.cgroupdriver=cgroupfs"],  
    "log-driver": "json-file",
    "insecure-registries": [
                            "192.168.2.100:5000","192.168.2.166:5000"
                                    ],
    "log-opts": {"max-size": "10m","max-file": "10"},
    "exec-opts": ["native.cgroupdriver=cgroupfs"],
    "registry-mirrors": ["http://f4ec4c27.m.daocloud.io","http://koala-dev:5000"],
    "runtimes": {
                "nvidia": {
                                "path": "/usr/bin/nvidia-container-runtime",
                                            "runtimeArgs": []
                                                        }
                    },
                         "exec-opts": ["native.cgroupdriver=cgroupfs"],
                         "default-runtime":"nvidia"
}
EOF

#Docker命令执行取消sudo
#获取用户名
USER=$(grep  '\/bin\/bash' /etc/passwd | egrep  -v  root|awk   -F  ':'  '{print  $1}')

sudo groupadd docker
sudo usermod -aG docker $USER
sudo gpasswd -a $USER docker
systemctl restart docker
docker run  -it  nvidia/opencl nvidia-smi
echo "退出终端重新登录即可"