#!/bin/bash
#检测网络是否通畅
ping  -w 5    qq.com  || {
echo  "网络异常,脚本终止"
exit 1
}
#开启ssh
sudo apt install openssh-server  -y
sudo apt install openssh-client curl  lib32ncurses5 lib32z1  -y
systemctl start ssh.service 
systemctl enable ssh.service 
#更换国内源

#识别Ubuntu16/18
ubuntu_ver=$(cat /etc/issue |awk '{print  $2}'|xargs | awk -F  "."  '{print  $1}')

#Ubuntu 16.04 
_ubuntu16  () { 
cat  > /etc/apt/sources.list  << 'EOF'
# 默认注释了源码镜像以提高 apt update 速度，如有需要可自行取消注释
deb https://mirrors.tuna.tsinghua.edu.cn/ubuntu/ xenial main restricted universe multiverse
# deb-src https://mirrors.tuna.tsinghua.edu.cn/ubuntu/ xenial main restricted universe multiverse
deb https://mirrors.tuna.tsinghua.edu.cn/ubuntu/ xenial-updates main restricted universe multiverse
# deb-src https://mirrors.tuna.tsinghua.edu.cn/ubuntu/ xenial-updates main restricted universe multiverse
deb https://mirrors.tuna.tsinghua.edu.cn/ubuntu/ xenial-backports main restricted universe multiverse
# deb-src https://mirrors.tuna.tsinghua.edu.cn/ubuntu/ xenial-backports main restricted universe multiverse
deb https://mirrors.tuna.tsinghua.edu.cn/ubuntu/ xenial-security main restricted universe multiverse
# deb-src https://mirrors.tuna.tsinghua.edu.cn/ubuntu/ xenial-security main restricted universe multiverse
# 预发布软件源，不建议启用
# deb https://mirrors.tuna.tsinghua.edu.cn/ubuntu/ xenial-proposed main restricted universe multiverse
# deb-src https://mirrors.tuna.tsinghua.edu.cn/ubuntu/ xenial-proposed main restricted universe multiverse

EOF

#开启tab键补齐
cat  >> /etc/bash.bashrc  << 'EOF'
#enable bash completion in interactive shells  
if ! shopt -oq posix; then  
if [ -f /usr/share/bash-completion/bash_completion ]; then  
. /usr/share/bash-completion/bash_completion  
elif [ -f /etc/bash_completion ]; then  
. /etc/bash_completion  
fi  
fi 
EOF
source   /etc/bash.bashrc

curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository \
   "deb [arch=amd64] https://mirrors.tuna.tsinghua.edu.cn/docker-ce/linux/ubuntu \
   $(lsb_release -cs) \
   stable"
sudo apt-get update
}

_ubuntu18  ()  {
cat  > /etc/apt/sources.list  << 'EOF'
# 默认注释了源码镜像以提高 apt update 速度，如有需要可自行取消注释
deb https://mirrors.tuna.tsinghua.edu.cn/ubuntu/ bionic main restricted universe multiverse
# deb-src https://mirrors.tuna.tsinghua.edu.cn/ubuntu/ bionic main restricted universe multiverse
deb https://mirrors.tuna.tsinghua.edu.cn/ubuntu/ bionic-updates main restricted universe multiverse
# deb-src https://mirrors.tuna.tsinghua.edu.cn/ubuntu/ bionic-updates main restricted universe multiverse
deb https://mirrors.tuna.tsinghua.edu.cn/ubuntu/ bionic-backports main restricted universe multiverse
# deb-src https://mirrors.tuna.tsinghua.edu.cn/ubuntu/ bionic-backports main restricted universe multiverse
deb https://mirrors.tuna.tsinghua.edu.cn/ubuntu/ bionic-security main restricted universe multiverse
# deb-src https://mirrors.tuna.tsinghua.edu.cn/ubuntu/ bionic-security main restricted universe multiverse
# 预发布软件源，不建议启用
# deb https://mirrors.tuna.tsinghua.edu.cn/ubuntu/ bionic-proposed main restricted universe multiverse
# deb-src https://mirrors.tuna.tsinghua.edu.cn/ubuntu/ bionic-proposed main restricted universe multiverse
EOF

curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository \
   "deb [arch=amd64] https://mirrors.tuna.tsinghua.edu.cn/docker-ce/linux/ubuntu \
   $(lsb_release -cs) \
   stable"
sudo apt-get update
}
#更新apt源
[ $ubuntu_ver -eq 16 ]  && _ubuntu16 && sudo apt update -y
[ $ubuntu_ver -eq 18 ]  && _ubuntu18 &&   sudo apt update -y

#安装显卡驱动所需依赖
apt install gcc make  -y
#屏蔽集显

cat   >> /etc/modprobe.d/blacklist-nouveau.conf   << 'EOF'
blacklist nouveau
options nouveau modeset=0
EOF

sudo update-initramfs -u
reboot
