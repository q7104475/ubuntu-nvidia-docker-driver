#!/bin.bash
#安装nvi-docker
sudo docker volume ls -q -f driver=nvidia-docker | xargs -r -I{} -n1 docker ps -q -a -f volume={} | xargs -r docker rm -f
sudo apt-get purge -y nvidia-docker
sudo apt autoremove


curl -s -L https://nvidia.github.io/nvidia-docker/gpgkey | \
sudo apt-key add -
distribution=$(. /etc/os-release;echo $ID$VERSION_ID)

# command 3
curl -s -L https://nvidia.github.io/nvidia-docker/$distribution/nvidia-docker.list | \
  sudo tee /etc/apt/sources.list.d/nvidia-docker.list
sudo apt-get update
 
# 正式安装。Install nvidia-docker2 and reload the Docker daemon configuration
sudo apt-get install -y nvidia-docker2
sudo pkill -SIGHUP dockerd
cat > /etc/docker/daemon.json  <<EOF


{  "registry-mirrors": ["http://f1361db2.m.daocloud.io"],
    "exec-opts": ["native.cgroupdriver=cgroupfs"],  
    "log-driver": "json-file",
    "log-opts": {"max-size": "10m","max-file": "10"},
    "exec-opts": ["native.cgroupdriver=cgroupfs"],
    "registry-mirrors": ["http://f4ec4c27.m.daocloud.io","http://koala-dev:5000"],
    "runtimes": {
                "nvidia": {
                                "path": "/usr/bin/nvidia-container-runtime",
                                            "runtimeArgs": []
                                                        }
                    },
                         "exec-opts": ["native.cgroupdriver=cgroupfs"],
                         "default-runtime":"nvidia"
}
EOF
systemctl restart docker
 
docker run  -it  nvidia/opencl nvidia-smi